import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
    providedIn: 'root'
})

export class PartnerService{
    constructor(
        private http: HttpClient
    ){   }

    getServicePromotion(){
        return this.http.get('/assets/partner.json');
    }
}