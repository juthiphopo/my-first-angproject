import { Component , OnInit } from '@angular/core';
import { PartnerService } from '../partner.service';

@Component({
    selector: 'app-promotion',
    templateUrl: 'promotion.component.html',
    styleUrls: ['promotion.component.css']
})

export class PromotionComponent implements OnInit{

    PartnerPromotion;
    
    constructor( private partnerService : PartnerService ){}

    ngOnInit(){
        this.PartnerPromotion = this.partnerService.getServicePromotion();
    }
}
