import {Component , OnInit} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import {products} from '../products';
import { CartService } from '../cart.service';
@Component({
    selector: 'app-product-list',
    templateUrl: './product-list.component.html',
    styleUrls: ['./product-list.component.css']
})

export class ProductListComponent implements OnInit {
    
    products = products;

    share(){
        window.alert('The Product has been share');
    }
    onNotify(){
        window.alert('Ok you will get notification')
    }
    product;

    constructor(
        private route: ActivatedRoute,
        private cartService: CartService
    ){}

    addToCart(product){
        window.alert('Your order has been add to cart');
        this.cartService.addToCart(product);
    }

    ngOnInit(){
        this.route.paramMap.subscribe(params => {
            this.product = products[+params.get('productId')];
        })
    }


}