import { Component , OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { products } from '../products';
import { CartService } from '../cart.service';

@Component({
    selector: 'app-product-details',
    templateUrl: './product-details.component.html',
    styleUrls: ['./product-details.component.css']
})

export class ProductDetailsComponent implements OnInit {
    product;

    onNotify(){
        window.alert('Ok you will get notification')
    }
    
    constructor(
        private route: ActivatedRoute,
        private cartService: CartService
    ){}

    addToCart(product){
        window.alert('Your order has been add to cart');
        this.cartService.addToCart(product);
    }
   

    ngOnInit(){
        this.route.paramMap.subscribe(params => {
            this.product = products[+params.get('productId')];
        })
    }
}