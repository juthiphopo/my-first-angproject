import { PathLocationStrategy } from '@angular/common';

export const products = [
    {        
        name: 'Phone XL',
        price: 799,
        description: 'Large phone screen from apple' ,
        image: 'mobile-phone.png' ,
        discount: 0,
        stock: 0
    },
    {
        name: 'Huaweii',
        price: 699,
        description: 'Large phone screen from android' ,
        image: 'mobile-phone.png' ,
        discount: 0,
        stock: 1
    },
    {
        name: 'Phone Sumsung',
        price: 600,
        description: 'Large phone screen from android' ,
        image: 'mobile-phone.png' ,
        discount: 15,
        stock: 2
    },
    {
        name: 'Phone Realme',
        price: 599,
        description: 'New Brand phone from android' ,
        image: 'mobile-phone.png' ,
        discount: 5,
        stock: 3
    },

]